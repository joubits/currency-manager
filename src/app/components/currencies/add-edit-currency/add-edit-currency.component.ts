import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency } from '../../../models/currency.model';
import { CurrencyService } from '../../../services/currency.service';

@Component({
  selector: 'app-add-edit-currency',
  templateUrl: './add-edit-currency.component.html',
  styleUrls: ['./add-edit-currency.component.scss']
})
export class AddEditCurrencyComponent implements OnInit {
  isEdit: boolean;
  currencyId: number;
  title: string;
  currencyToEdit: Currency = {
      id: 0,
      country: '' ,
      currencyName: '',
      currencyCode: '',
      currencySymbol: '',
      displayCodeSymbol: null, 
      displayBeforeAfterPrice: null,
      displayCents: null,
      displayCommaDotDecimalSeparator: null
  };

  constructor(private router: Router, private route: ActivatedRoute, private currencyService: CurrencyService) { }

  ngOnInit(): void {
    this.currencyId = this.parseIntBase10(this.route.snapshot.paramMap.get('id'));
    this.currencyId === 0 ? this.isEdit = false : this.isEdit = true;
    if(!this.isEdit) {
      this.title = 'Add' ;
    } else {
      this.title = 'Edit';
      this.currencyToEdit = this.currencyService.onGetCurrency(this.currencyId);
    }
  }
  
  onSubmit(form: NgForm) {
    const formCurrency = {
      id: Math.floor(Math.random() * 1000), // unique id
      country: form.value.currencycountry ,
      currencyName: form.value.currencyname,
      currencyCode: form.value.currencycode,
      currencySymbol: form.value.currencysymbol,
      displayCodeSymbol: this.parseIntBase10(form.value.currencyCodeSymbol) === 1 ? true : this.parseIntBase10(form.value.currencyCodeSymbol) === 0 ? false : null, 
      displayBeforeAfterPrice: this.parseIntBase10(form.value.beforeafterprice) === 1 ? true : this.parseIntBase10(form.value.beforeafterprice) === 0 ? false: null,
      displayCents: this.parseIntBase10(form.value.centsnocents) === 1 ? true : this.parseIntBase10(form.value.centsnocents) === 0 ? false : null,
      displayCommaDotDecimalSeparator: this.parseIntBase10(form.value.currencyformat) === 1 ? true : this.parseIntBase10(form.value.currencyformat) === 0 ? false : null
    };
    
    if(!this.isEdit) {
      this.currencyService.onAdd(formCurrency);
    } else {
      formCurrency.id = this.currencyToEdit.id
      this.currencyService.onUpdate(formCurrency);
    }
    this.router.navigateByUrl('');
  }

  parseIntBase10(numberString: string) {
    return parseInt(numberString, 10);
  }

}

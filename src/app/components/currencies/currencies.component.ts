import { Component, Directive, EventEmitter, Input, Output, QueryList, ViewChildren, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Currency } from '../../models/currency.model';
import { CurrencyService } from '../../services/currency.service';
import { ExportService } from '../../services/export.service';

export type SortColumn = keyof Currency | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss']
})
export class CurrenciesComponent implements OnInit {
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  @ViewChild('currencyTable') currencyTable: ElementRef;

  currencyList: Currency[];
  currenciesTable: Currency[];

  constructor(private currencyService: CurrencyService, private exportService: ExportService) { }

  ngOnInit(): void {
    this.currencyList = this.currencyService.getAllCurrencies();
    this.currenciesTable = this.currencyList;
  }

  deleteCurrency(currencyId: number) {
    console.log('Currency to delete', currencyId);
    this.currencyService.onDelete(currencyId);
  }

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '' || column === '') {
      this.currenciesTable = this.currencyList;
    } else {
      this.currenciesTable = [...this.currencyList].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }
  // export to xlsx
  exportElmToExcel(): void {
    this.exportService.exportTableElmToExcel(this.currencyTable, 'currency_table_data');
  }
  // export to csv
  exportToCsv(): void {
    this.exportService.exportToCsv(this.currencyList, 'currency_table_data', ['id', 'country', 'currencyName', 'currencyCode', 'currencySymbol']);
  }

}

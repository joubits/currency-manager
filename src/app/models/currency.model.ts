export class Currency {
    public id: number;
    public country: string;
    public currencyName: string;
    public currencyCode: string;
    public currencySymbol: string;
    public displayCodeSymbol: boolean; // true Code, false Symbol
    public displayBeforeAfterPrice: boolean; // true Before, false After
    public displayCents; // true Cents, false No Cents
    public displayCommaDotDecimalSeparator; // true Comma, false dot
}
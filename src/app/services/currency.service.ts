import { Injectable } from '@angular/core';
import { Currency } from '../models/currency.model';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  
    curriencies: Currency[] = [
      {
        id: 1,
        country: 'Germany',
        currencyName: 'EURO',
        currencyCode: 'EUR',
        currencySymbol: '€',
        displayCodeSymbol: false,
        displayBeforeAfterPrice: true,
        displayCents: false,
        displayCommaDotDecimalSeparator: false
      },
      {
        id: 2,
        country: 'Argentina',
        currencyName: 'US dollar',
        currencyCode: 'USD',
        currencySymbol: '$',
        displayCodeSymbol: true,
        displayBeforeAfterPrice: true,
        displayCents: false,
        displayCommaDotDecimalSeparator: true
      }
    ];

  constructor() { }
  // get all currencies
  getAllCurrencies() {
    return this.curriencies;
  }
  // get a currency by id
  onGetCurrency(currencyId: number) {
    return this.curriencies.find(c => c.id === currencyId);
  }
  // save or add new currency
  onAdd(newCurrency: Currency) {
    this.curriencies.push(newCurrency);
  }
  // remove a currency
  onDelete(currencyId: number) {
    const currency = this.curriencies.find((x) => x.id === currencyId);
    const index = this.curriencies.indexOf(currency);
    if (index !== -1) {
      this.curriencies.splice(index, 1);
    }
    
  }

  onUpdate(currency: Currency) {
    const oldCurrency = this.curriencies.find((c => c.id === currency.id));
    oldCurrency.country = currency.country;
    oldCurrency.currencyName = currency.currencyName;
    oldCurrency.currencyCode = currency.currencyCode;
    oldCurrency.currencySymbol = currency.currencySymbol;
    oldCurrency.displayCodeSymbol = currency.displayCodeSymbol;
    oldCurrency.displayBeforeAfterPrice = currency.displayBeforeAfterPrice;
    oldCurrency.displayCents = currency.displayCents;
    oldCurrency.displayCommaDotDecimalSeparator = currency.displayCommaDotDecimalSeparator; 

  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrenciesComponent } from './components/currencies/currencies.component';
import { AddEditCurrencyComponent } from './components/currencies/add-edit-currency/add-edit-currency.component';


const routes: Routes = [
  {
    path: '',
    component: CurrenciesComponent
  },
  {
    path: 'currency/add/:id',
    component: AddEditCurrencyComponent
  },
  {
    path: 'currency/edit/:id',
    component: AddEditCurrencyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
